import os
import glob
from bs4 import BeautifulSoup
from datetime import datetime
import time
import pandas as pd

# Table format:
# Time, Wind Dir, Wind Spd, Wind Gust, Temp, Dew, Feels like, Humidity, Fire, Rain, Rain 10', Pressure
# EDT , 		, km/hour , km/hour  , C   , C  , C         , %       ,     , mm  , mm      , hPa

def parse_time(year, month, day, hour, minute):
	"""Auto fill 0 for seconds"""
	date = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':00'
	dateformat = "%Y-%m-%d %H:%M:%S"
	timestamp = time.strptime(date, dateformat)
	timestamp = int(time.mktime(timestamp))
	return timestamp

def main():
	data_directory = './data'
	filename = 'Viewbank 24 hour history graph of temperature, wind and rainfall.html'

	os.chdir(data_directory)

	for subfold in glob.glob("*"):
		print(subfold)
		temp_string = subfold.split("_")
		location = (temp_string[0])
		daystamp = (temp_string[1])
		year = daystamp[0:4]
		month = daystamp[4:6]
		day = daystamp[6:]

		filepath = subfold + '\\' + filename
		print('location: ' + location)
		print('date: ' + daystamp)
		with open(filepath, encoding='utf8') as infile:

			soup = BeautifulSoup(infile, 'html.parser')

		# find all <td></td> tags
		tds = soup.find_all('td')

		weather = {
			'date':[],
			'wind_dir':[],
			'wind_spd':[],
			'wind_gus':[],
			'temp':[],
			'dew':[],
			'feels':[],
			'humidity':[],
			'fire':[],
			'rain':[],
			'rain10':[],
			'pressure':[],
		}

		# ignore first row since it is next day
		for i in range(1,int(len(tds)/12)):
			clock = tds[i*12+0].contents[0]
			clock = clock.split(" ")[1]
			hour, minute = clock.split(":")
			
			weather['date'].append(parse_time(year, month, day, hour, minute))

			weather['wind_dir'].append(tds[i*12+1].contents[0])
			weather['wind_spd'].append(tds[i*12+2].contents[0])
			weather['wind_gus'].append(tds[i*12+3].contents[0])
			weather['temp'].append(tds[i*12+4].contents[0])
			weather['dew'].append(tds[i*12+5].contents[0])
			weather['feels'].append(tds[i*12+6].contents[0])
			weather['humidity'].append(tds[i*12+7].contents[0])
			weather['fire'].append(tds[i*12+8].contents[0])
			weather['rain'].append(tds[i*12+9].contents[0])
			weather['rain10'].append(tds[i*12+10].contents[0])
			weather['pressure'].append(tds[i*12+11].contents[0])

		# export dataframe to csv
		df = pd.DataFrame.from_dict(weather)
		column_names = ['date', 'temp']
		df.to_csv(subfold + '.csv', columns=column_names, index = False)

if __name__ == '__main__':
	main()